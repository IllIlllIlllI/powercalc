MAKEFLAGS += -rR --no-print-directory

NPROCS   := 1
OS       := $(shell uname)

ifeq ($(OS),Linux)
  NPROCS := $(shell grep -c ^processor /proc/cpuinfo)
else ifeq ($(OS),Darwin)
endif

all:
	@$(MAKE) -f src/Makefile $@

release:
	@$(MAKE) -f src/Makefile $@

clean:
	@$(MAKE) -f src/Makefile $@

%:
	@$(MAKE) -f src/Makefile $@
