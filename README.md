Fairly useless program for calculating power costs.

Download here: http://usernamestaken.github.com/powercalc/

Initially started as a companion tool for oclHashcat-plus/lite but has use for anyone curious about their consumption.  Run make to build yourself, or use one of the binaries included in /release.

At some point I may write a GUI for this, or if someone actually finds it useful, can do it themself.
