/*
  powercalc.c
  Copyright (c) 2012 - 2013, radix (radix@hashcat.net)
  All rights reserved.
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdint.h>
#include <unistd.h>

#define PROGNAME "powercalc"
#define VERSION "v0.04"
#define AUTHOR "radix"

int main(int argc, char **argv)
{

  int j;

  static struct option long_options[] =
  {
    {"help",     no_argument, 0, 'h'},
    {"guided",   no_argument, 0, 'g'},
    {"advanced", no_argument, 0, 'a'},
    {"initial",  no_argument, 0, 'i'},
    {0, 0, 0, 0}
  };

  int option_index = 0;

  if (argc < 2)
  {
    printf("%s %s by %s.\nUse --guided or --help to get started.\n", PROGNAME, VERSION, AUTHOR);
    exit (0);
  }

  while((j = getopt_long (argc, argv, "hgai", long_options, &option_index)) != -1)
  {
    switch (j)
    {
    case 0:
      break;
    case 'h':
      printf("%s %s by %s.\n", PROGNAME, VERSION, AUTHOR);
      printf("Usage:\n");
      printf("-h or --help           prints this message\n");
      printf("-g or --guided         starts question/answer mode\n");
      printf("-a or --advanced       used to specify options by command line only\n");
      printf("-i or --initial        calculates cost per kWh\n\n");
      printf("CLI mode options (assumes -a or -i):\n\n");
      printf("advanced options:\n\n");
      printf("-w or --watts          specifies watt load\n");
      printf("-c or --cost           specifies known kWh cost\n");
      printf("-t or --time           specifies number of hours run\n\n");
      printf("initial options:\n\n");
      printf("-k or --bill_kWh       number of kWh used from your powerbill\n");
      printf("-b or --bill_cost      amount paid for powerbill\n");
      break;
    case 'g':
      guided();
      break;
    case 'a':
      advanced(argc, argv);
      break;
    case 'i':
      initial(argc, argv);
      break;
    default: /* '?' */
      fprintf(stderr, "Usage: %s --guided or --help\n",
      argv[0]);
      exit(EXIT_FAILURE);
    }
  }
}

int guided()
{

  float watts;
  float cost;
  float hours;
  float bill_kWh;
  float bill_cost;
  char calc;
  char calc1;
  char calc2;
  char s[4];
  int c;

  printf("%s %s by %s.\n\n", PROGNAME, VERSION, AUTHOR);
  printf("Do you know your kWh cost? (y/n):");
  scanf("%c", &calc);

  if (calc == 'n')
  {
    printf("Enter number of kWh used: ");
    while(scanf("%f", &bill_kWh) != 1)
    {
      printf("Expected an integer value but got something else.\nEnter number of kWh used: ");
      while((c = getchar()) != EOF && c != '\n');
    }
    if (bill_kWh <= 0)
    {
      printf("kWh must be greater than 0\n");
      return -1;
    }
    printf("\nEnter amount paid: ");
    while(scanf("%f", &bill_cost) != 1)
    {
      printf("Expected an integer value but got something else.\nEnter amount paid: ");
      while((c = getchar()) != EOF && c != '\n');
    }
    if (bill_cost <= 0)
    {
      printf("Cost must be greater than 0");
      return -1;
    }
    printf("\nEnter 3 char currency code: ");
    scanf("%3s", s);
    {
      printf("\nYour approximate cost per kWh is %f %s\n", bill_cost / bill_kWh, s);
      return 0;
    }
  }

  else if(calc == 'y')
  {
    printf("Enter watts at load: ");
    while(scanf("%f", &watts) != 1)
    {
      printf("Expected an integer value but got something else.\nEnter watts at load: ");
      while((c = getchar()) != EOF && c != '\n');
    }
    if (watts <= 0)
    {
      printf("Watts must be greater than 0");
      return -1;
    }
    printf("\nEnter cost per kWh: ");
    while(scanf("%f", &cost) != 1)
    {
      printf("Expected an integer value but got something else.\nEnter cost per kWh: ");
      while((c = getchar()) != EOF && c != '\n');
    }
    if (cost <= 0)
    {
      printf("Cost must be greater than 0");
      return -1;
    }
    printf("\nEnter estimated time in hours: ");
    while(scanf("%f", &hours) != 1)
    {
      printf("Expected an integer value but got something else.\nEnter estimated time in hours: ");
      while((c = getchar()) != EOF && c != '\n');
    }
    if (hours <= 0)
    {
      printf("Hours must be greater than 0");
      return -1;
    }
    printf("\nEnter 3 char currency code: ");
    scanf("%3s", s);
    printf("\nYour estimated cost is %.4f %s\n", (watts / 1000.0) * cost * hours, s);
    return 0;
  }
}

int advanced(int argc, char **argv)
{
  int a;
  float watts;
  float cost;
  float time;

  static struct option long_options[] =
  {
    {"watts",  required_argument, 0, 'w'},
    {"cost",   required_argument, 0, 'c'},
    {"time",   required_argument, 0, 't'},
    {0, 0, 0, 0}
  };
  int option_index = 1;

  if (argc < 7)
  {
    printf("%s %s by %s.\nUse --guided or --help to get started.\n", PROGNAME, VERSION, AUTHOR);
    exit (0);
  }

  while((a = getopt_long (argc, argv, "w:c:t:", long_options, &option_index)) != -1)
  {
    switch (a)
    {
    case 'w':
      watts = atof(optarg);
      break;
    case 'c':
      cost = atof(optarg);
      break;
    case 't':
      time = atof(optarg);
      break;
    default: /*'?'*/
      fprintf(stderr, "Usage: %s\n",
      argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  {
    printf("%s %s by %s.\n", PROGNAME, VERSION, AUTHOR);
    printf("\nYour estimated cost is %.4f\n", (watts / 1000.0) * cost * time);
    return 0;
  }
}

int initial(int argc, char**argv)

{
  int i;
  float bill_kWh;
  float bill_cost;

  static struct option long_options[] =
  {
    {"bill_kWh",    required_argument, 0, 'k'},
    {"bill_cost",    required_argument, 0, 'b'},
    {0, 0, 0, 0}
  };

  int option_index = 2;

  if (argc < 5)
  {
    printf("%s %s by %s.\nUse --guided or --help to get started.\n", PROGNAME, VERSION, AUTHOR);
    exit (0);
  }

  while((i = getopt_long (argc, argv, "k:b:", long_options, &option_index)) != -1)
  {
    switch (i)
    {
    case 0:
      break;
    case 'k':
      bill_kWh = atof(optarg);
      break;
    case 'b':
      bill_cost = atof(optarg);
      break;
    default: /*'?'*/
      fprintf(stderr, "Usage: %s --guided or --help\n",
      argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  {
    printf("%s %s by %s.\n", PROGNAME, VERSION, AUTHOR);
    printf("\nYour approximate cost per kWh is %f\n", bill_cost / bill_kWh);
    return 0;
  }
}
